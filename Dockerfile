# MediaWiki Docker image
#
# Copyright (C) 2019-2020 Libre Space Foundation <https://libre.space/>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

ARG MEDIAWIKI_VERSION
FROM mediawiki:${MEDIAWIKI_VERSION}
MAINTAINER LSF operations team <ops@libre.space>

RUN apt-get update \
	&& apt-get install -y --no-install-recommends \
		zip \
		unzip \
	&& rm -r /var/lib/apt/lists/*

# Install Chameleon skin and OpenIDConnect library
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir="/usr/local/bin" --filename="composer" --1 \
	&& composer require mediawiki/chameleon-skin:3.4.3 jumbojett/openid-connect-php:v0.8.0 \
	&& rm /usr/local/bin/composer

# Install VisualEditor, PluggableAuth and OpenIDConnect extensions
RUN _branch="REL$(echo "$MEDIAWIKI_MAJOR_VERSION" | sed 's/\./_/g')" && \
	for _extension in PluggableAuth OpenIDConnect; do \
		git clone --depth 1 -b "${_branch}" "https://github.com/wikimedia/mediawiki-extensions-${_extension}.git" extensions/${_extension}; \
		rm -rf extensions/${_extension}/.git/; \
	done;
